(ns code-samples.extract-or-clauses-test
  (:use clojure.test)
  (:use :reload-all code-samples.extract-or-clauses))

(deftest glue-pairs-simple
  (is (= '((1 3) (5 7))
         (glue-pairs [[1 2] [2 3] [5 6] [6 7]]))))

(deftest test-no-or
  (is (= '(:a :b)
         (extract-or-clauses [:a :b]))))

(deftest test-single-or
  (is (= '((:or :a :b))
         (extract-or-clauses [:a :or :b]))))

(deftest test-multiple-ors-1
  (is (= '((:or :a :b :c))
         (extract-or-clauses [:a :or :b :or :c]))))

(deftest test-multiple-ors-2
  (is (= '((:or :a :b) (:or :c :d))
         (extract-or-clauses [:a :or :b :c :or :d]))))








