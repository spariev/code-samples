(ns code-samples.query-parser-test
  (:use clojure.test)
  (:use :reload-all code-samples.query-parser))

(def fiz-baz-value
     [{:exact false :wildcard false :term "fiz"}
      {:exact false :wildcard false :term "baz"}])

(def chunky-bacon-value
     [{:exact false :wildcard false :term "chunky"}
      {:exact false :wildcard false :term "bacon"}])

(deftest test-single-clause
  (is (= {:type :+ :value [{:exact false  :wildcard false :term "test"}]}
	 (first (parse-query "(test)")))))

(deftest test-exact-clause
  (is (= {:type :+ :value [{:exact true :wildcard false :term "why of Y"}]}
	 (first (parse-query "(\"why of Y\")")))))

(deftest test-multi-exact-clause
  (is (= {:type :+ :value [{:exact false :wildcard false :term "richard"}
			   {:exact false :wildcard false :term "gabriel"}
			   {:exact true  :wildcard false
			    :term "software patterns"}]}
	 (first (parse-query
		 "(richard gabriel \"software patterns\")")))))

(deftest test-multiple-clauses
  (is (= [{:type :+ :value fiz-baz-value}
	  :or {:type :+ :value chunky-bacon-value}]
	 (parse-query "(fiz baz) | (chunky bacon)")))
  (is (= [{:type :+ :value fiz-baz-value}
	  :or {:type :- :value chunky-bacon-value}]
	 (parse-query "(fiz baz) | -(chunky bacon)"))))

(deftest test-multiple-clauses-no-or
  (is (= [{:type :+ :value fiz-baz-value}
	  {:type :- :value chunky-bacon-value}]
	 (parse-query "(fiz baz) -(chunky bacon)"))))

(deftest test-star
  (is (= [{:type :+ :value :all}] (parse-query "*"))))

(deftest test-simple-query
  (is (= [{:type :+
	   :value [{:exact false, :wildcard false, :term "simple query"}]}]
	 (parse-query " simple query "))))

(deftest test-wildcard-query
  (is (= [{:type :+ :value
	   [{:exact false
	     :wildcard true
	     :term "*simple*"}]}] (parse-query "(*simple*)"))))

(deftest test-multiclauses-no-parens
  (is (= [{:type :+ :value [{:exact false :wildcard false :term "simple"}
			    {:exact false :wildcard false :term "query"}]}
	  :or
	  {:type :+ :value [{:exact false :wildcard false :term "fiz"}
			    {:exact false :wildcard false :term "baz"}]}]
	 (parse-query "simple query | fiz baz"))))

(deftest test-multiclauses-simple-and-parens
  (is (= [{:type :+ :value [{:exact false :wildcard false :term "simple"}]}
	  {:type :+ :value [{:exact false :wildcard false :term "query"}]}
	  :or
	  {:type :+ :value [{:exact false :wildcard false :term "fiz"}
			    {:exact false :wildcard false :term "baz"}]}]
	 (parse-query "simple (query) | (fiz baz)")))
  (is (= [{:type :+ :value [{:exact false :wildcard false :term "simple"}]}
	  {:type :+ :value [{:exact false :wildcard false :term "query"}]}
	  :or
	  {:type :+ :value [{:exact false :wildcard false :term "fiz"}]}
	  {:type :+ :value [{:exact false :wildcard false :term "baz"}]}]
	 (parse-query "(simple) (query) | (fiz) baz"))))

(deftest test-field-list-simple
  (is (= [{:fields "foo,bar"}
	  {:type :+ :value [{:exact false :wildcard false :term "simple"}
			    {:exact false :wildcard false :term "query"}]}]
	 (parse-query "@(foo,bar) simple query"))))


(deftest test-field-list-parens-no-or-exact
  (is (= [{:fields "foo,bar"}
	  {:type :+ :value [{:exact false :wildcard false :term "simple"}
			    {:exact false :wildcard false :term "query"}]}
	  :or
	  {:type :+ :value [{:exact true :wildcard false :term "fiz baz"}]}
	  {:type :- :value [{:exact false :wildcard false :term "chunky"}]}
	  ]
	 (parse-query "@(foo,bar) (simple query) | \"fiz baz\" -(chunky)"))))
