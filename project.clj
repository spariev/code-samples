(defproject code-samples "0.1.0-SNAPSHOT"
  :description "Clojure Code Samples"
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.2.0"]
                 [fnparse "2.2.7"]])
