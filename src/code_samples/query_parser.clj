(ns code-samples.query-parser
  (require [clojure.string :as str2])
  (:use name.choi.joshua.fnparse))

(defn if-char->seq
  [val]
  (if (instance? Character val) [(str val)]
      val))
(defn ->s [content]
  (.trim #^String (apply str (if-char->seq content))))

(defn cond-type-by-open-paren
  [open-paren]
  (let [op-str (->s (flatten [open-paren]))]
    (cond (or (nil? op-str) (= "(" op-str)) :+
          (= "-(" op-str) :-
          :else :+)))

(defn wildcard-term?
  [#^String term]
  (< -1 (.indexOf term "*")))

(def ws
  (complex [_ (alt (lit \space) (lit \tab))]
           :space))

(def close-paren-lit (lit \)))

(def open-paren-lit (lit \())

(def open-paren-not-lit (lit-conc-seq [\- \(]))

(def double-quote-lit (lit \"))

(def open-paren-fields-lit (lit-conc-seq [\@ \(]))

(def or-lit
     (complex [_ (lit \|)]
              :or))

(defn simple-query-validator
 [qy]
 (let [#^String q (apply str qy)]
   (every? #(= -1 (.indexOf q #^String %)) ["*" "|" "-(" "(" ")"])))

(def simple-query-lit
     (complex [content (validate
                        (rep+ anything)
                        simple-query-validator)]
                [{:type :+ :value [{ :exact false :wildcard false
                                   :term (->s content)}]}]))

(def fields-list-lit
     (complex [_ open-paren-fields-lit
               content (rep+ (except anything close-paren-lit))
               _ close-paren-lit]
              (let [fields-list (->s content)]
                {:fields fields-list})))

(def term-lit
     (complex [content (rep+ (except anything
                                     (alt ws or-lit open-paren-lit
                                          open-paren-not-lit close-paren-lit)))]

              (let [term (->s content)]
                {:exact false :wildcard (wildcard-term? term)
                 :term term})))

(def quoted-term-lit
     (complex [_ double-quote-lit
               content (rep+ (except anything double-quote-lit))
               _ double-quote-lit]
              (let [term (->s content)]
                {:exact true :wildcard false :term term})))

(def term-list
  (rep+ (alt quoted-term-lit term-lit ws)))

(def query-clause-lit
  (complex
   [open-paren (opt (alt open-paren-not-lit open-paren-lit))
    content    (except term-list
                       (if open-paren
                         close-paren-lit
                         (alt ws or-lit)))
    _          (if open-paren
                 close-paren-lit
                 (opt close-paren-lit))]
   {:type (cond-type-by-open-paren open-paren)
    :value (vec (remove #{:space} (flatten content)))}))

(def star-lit
  (complex [_ (lit \*)]
           [{:type :+ :value :all}]))

(def root-rule
   (alt star-lit
        simple-query-lit
        (rep* (alt ws fields-list-lit or-lit query-clause-lit))))

(defn cleanup-query
  [string-val]
  (reduce #(.replace #^String %1 #^CharSequence (first %2) #^CharSequence (last %2))
          string-val (seq {"\\ " " "
                           "\\." "."
                           "\\(" ""
                           "\\)" ""
                           "\\-" "-"
                           "\\+" " "
                           "&" " "
                           "\\|" "|"})))
(defn- prepare-query
  [#^String string-value]
  (if (and string-value (= 0 (.length string-value)))
    "*"
    (cleanup-query string-value)))

(defn match-rule
  [rule str]
  (vec (remove
        #(= :space %)
        (rule-match rule
                    #(println "FAILED: " %)
                    #(do (println "TOKENS LEFT:" %2) [{:type :- :value :none}])
                    { :remainder
                     (seq str)}))))

(defn parse-query
  "Returns a seq of query clauses."
  [string-value]
  (match-rule root-rule (prepare-query string-value)))