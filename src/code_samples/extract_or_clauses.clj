(ns code-samples.extract-or-clauses
  (:use [clojure.contrib.seq-utils :only [indexed]]))

(defn in-range?
  [i [lower upper]]
  (<= lower i upper))

(defn glue-pairs
  "accepts a seq of pairs like [[1 2] [2 3] [5 6] [6 7]]
   and glues them into longest possible subsequences, so example above becomes
   ((1 3) (5 7))"
  [pairs]
  (let [idxs (flatten pairs)
        freqs (frequencies idxs)]
    (partition-all 2
                   (filter #(= 1 (freqs %)) idxs))))

(defn or-clause-from-range
  "applies glues or ranges to conditions array
   and moves :or to the head of the result.
   ie [:a :or :b :c :or :d] with range  [0 2]
   becomes (:or :a :b)"
  [rng conditions]
  (flatten [:or (remove #(= :or %)
                        (subvec (vec conditions) (first rng) (inc (last rng))))]))

(defn extract-or-clauses
  "rearranges conditions with :or, so :or comes to the head of the or-ed group of conditions. see tests for more insight"
  [conditions]
  (let [or-ed-ranges (glue-pairs (->> (indexed conditions)
                                     (filter #(= :or (last %)))
                                     (map first)
                                     (map (fn [i] [(dec i) (inc i)]))))
        or-range-for-idx (fn [idx] (first
                                    (filter
                                     (partial in-range? idx) or-ed-ranges)))]
    (distinct
     (->> (indexed conditions)
          (map #(let [or-clause (or-range-for-idx (first %))]
                  (if or-clause
                    (or-clause-from-range or-clause conditions)
                    (last %))))))))
