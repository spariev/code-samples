# code-samples

There are 2 files with code samples from our custom Lucene based search application I developed, a bit anonimyzed and cleaned up.

Test could be run with lein2 test.

# query-parser

*code-samples.query-parser* is a FTS query parser based on functional parser library called [FnParse](https://github.com/joshua-choi/fnparse), implementing some subset of Sphinx Search [query syntax](http://sphinxsearch.com/docs/1.10/extended-syntax.html).

The main function is a *parse-query*.
Basically, query string from client gets cleaned up and parsed into data structure which then transformed into Lucene queries by the other parts of the system.
See tests in test/code_samples/query_parser_test.clj for more detailed usage samples, but basically query like
```
  "(fiz baz) | -(chunky bacon)"
```
ends up transformed into
```
  [{:type :+ :value [{:exact false :wildcard false :term "fiz"}
                     {:exact false :wildcard false :term "baz"}]}
   :or
   {:type :- :value [{:exact false :wildcard false :term "chunky"}
                     {:exact false :wildcard false :term "bacon"}]}]
```

# extract-or-clauses

*code-samples.extract-or-clauses* contains a set of functions, main entry point being *extract-or-clauses*, which help transform conditions received from query parser into the structure more suitable for generating a hierarchy of Lucene BooleanQuery-es later.

Conditions are splitted into OR-ed groups with :or prepended to the head of each group. Again, see comments to the functions and tests in test/code_samples/extract_or_clauses_test.clj for more insight.

